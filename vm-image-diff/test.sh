#!/bin/sh -eux

set -o pipefail

DOCKER_AUTH_CONFIG="${DOCKER_AUTH_CONFIG:-}"

GUESTFS_REPO="${GUESTFS_REPO:-https://gitlab.com/testing-farm/guestfs-tools.git}"
GUESTFS_BRANCH="${GUESTFS_BRANCH:-tft-ux-improvement}"

if [ -n "$DOCKER_AUTH_CONFIG" ]; then
    mkdir -p $HOME/.docker
    echo "$DOCKER_AUTH_CONFIG" > $HOME/.docker/config.json
fi

systemctl start docker
systemctl start libvirtd

tmpdir="$(mktemp -d -p /)"

pushd "$tmpdir"

# TODO: building virt-diff shall be replaced by prebuilt binary
git clone --depth 1 --branch "$GUESTFS_BRANCH" "$GUESTFS_REPO"

cd guestfs-tools
git checkout "$GUESTFS_BRANCH"
git reset --hard "origin/$GUESTFS_BRANCH"
git submodule init
git submodule update

autoreconf -i
./configure --disable-ocaml --disable-perl

make -C common/utils
make -C common/options
make -C common/visit
make -C common/structs
make -C gnulib/lib
make -C diff/

cp diff/virt-diff /usr/local/bin/

popd

# End of build phase, here comes the actual test.

LEFT_IMAGE_FILENAME="$LEFT_COMPOSE.$LEFT_COMPOSE_FORMAT"
RIGHT_IMAGE_FILENAME="$RIGHT_COMPOSE.$RIGHT_COMPOSE_FORMAT"

LEFT_IMAGE_FILEPATH="$tmpdir/$LEFT_IMAGE_FILENAME"
RIGHT_IMAGE_FILEPATH="$tmpdir/$RIGHT_IMAGE_FILENAME"

docker pull "$CNC_IMAGE"

docker run --rm -v "$tmpdir":/HOST:Z --user 0:0 "$CNC_IMAGE" bash -c "\
       tft-admin cloud set $LEFT_CLOUD \
    && tft-admin cloud compose download --target-file /HOST/$LEFT_IMAGE_FILENAME --format $LEFT_COMPOSE_FORMAT $LEFT_COMPOSE"

docker run --rm -v "$tmpdir":/HOST:Z --user 0:0 "$CNC_IMAGE" bash -c "\
       tft-admin cloud set $RIGHT_CLOUD \
    && tft-admin cloud compose download --target-file /HOST/$RIGHT_IMAGE_FILENAME --format $RIGHT_COMPOSE_FORMAT $RIGHT_COMPOSE"

chmod a+rx "$tmpdir"
chmod a+r "$LEFT_IMAGE_FILEPATH"
chmod a+r "$RIGHT_IMAGE_FILEPATH"

/usr/local/bin/virt-diff --format="$LEFT_COMPOSE_FORMAT" \
                         -a "$LEFT_IMAGE_FILEPATH" \
                         --format="$RIGHT_COMPOSE_FORMAT" \
                         -A "$RIGHT_IMAGE_FILEPATH" \
                         --extra-stats \
                         --uids \
                         --xattrs \
                         --ignore-atime \
                         --ignore-ctime \
                         --ignore-mtime \
                         --ignore-st-dev \
                         --ignore-st-ino \
                         --ignore-st-blocks-dir \
                         --ignore-st-size-dir \
                         --ignore-path "/var/cache.*" \
                         --ignore-path "/var/log/.*" \
                         --show-ignored |& tee diff.txt
