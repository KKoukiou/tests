#!/bin/bash
set -exo pipefail

systemctl start docker

if [ -n "$PACKAGES" ]; then
    dnf install -y $PACKAGES
fi

if [ -n "$DOCKER_AUTH_CONFIG" ]; then
    mkdir -p $HOME/.docker
    echo "$DOCKER_AUTH_CONFIG" > $HOME/.docker/config.json
fi

if [ -n "$VAULT_SECRET_ID" ]; then
    VAULT_ARGS="-e VAULT_SECRET_ID=$VAULT_SECRET_ID"
fi

curl -fsSL https://goss.rocks/install | sh
docker pull $IMAGE

for GOSS in ${GOSS_YAML/,/ }; do
    curl -Lo goss.yaml $GOSS
    dgoss run $VAULT_ARGS --rm -it --entrypoint bash --user root $IMAGE
done
